;(function ($) {
	"use strict";
	Drupal.behaviors.multisiteOverview = {
	attach: function (context, settings) {

		var sitelist = Drupal.settings.multisiteOverview.sites;
		var opensite = function (site_url) {
			var site_iframe = $('<fieldset class="collapsible form-wrapper collapsed" />'),
			    height =  $('#edit-height').val()+'px';
			site_iframe.append( $('<legend><span class="fieldset-legend">'+site_url+'</span></legend>\
<ul class="action-links">\
<li><a href="'+site_url+'" target="_blank" style="float:right">NEW WINDOW</a></li>\
</ul>\
<div class="clearfix"><br></div>\
<div class="fieldset-wrapper"><iframe style="width:100%;min-height:'+height+';height:'+height+'" src="'+site_url+'"/></div>') );
			$('#multisite-stage').append(site_iframe);
		}

		$("#edit-submit").click(function(event) {
			event.preventDefault();
			$('#multisite-stage').html('');
			var scheme =  $('#edit-scheme option:selected').text();
			for (var site_id in sitelist) {
				opensite(scheme+'://' + sitelist[site_id] + '/' + $('#edit-onsite-url').attr('value'));
			}
			Drupal.behaviors.collapse.attach($('#multisite-stage'));
		});

	}
};
}(jQuery));
