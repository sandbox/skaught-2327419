<?php

/**
 * @file
 * Admin page callbacks
 */

/**
 * Menu callback;
 */
function multisite_overview_settings($form, $form_state) {
	global $base_path;
	$sitesPath = DRUPAL_ROOT.$base_path.'sites';
	$form = array();

	$folderSites = multisite_overview_list_directory($sitesPath);
	if (count($folderSites)>0){
		/*
		// for future ref
		$drupalSites = FALSE;
		$sitesFile = $sitesPath.'/sites.php';
		if(is_file($sitesFile)) {
			global $sites;
			multisite_overview_global_include($sitesFile);
			$groups = array_flip($sites);
				//dpm(array_unique($sites));	
				//dpm($groups);	
				//dpm($folderSites);	
		}
		*/
		$types = multisite_overview_scheme_opts();
			$form['height'] = array(
			'#title'=>t('iFrame Height'),
			'#weight' => -2,
			'#type'=>(module_exists('elements')) ?'numberfield' : 'textfield',
			'#options'=>$types,
			'#size'=>10,
			'#field_suffix'=>'px',
			'#suffix'=>'</div>',
			'#prefix'=>'<div class="height-wrap">',
			'#maxlength'=>4,
			'#attributes'=>array(
					'maxlength'=>'4',
					'step'=>'50',
					'min'=>'100',
					'max'=>'1800',
				),
			'#default_value'=>'500',
		);
				$form['scheme'] = array(
			'#title'=>t('URI scheme'),
			'#weight' => -1,
			'#suffix'=>'</div><div class="clearfix"></div>',
			'#prefix'=>'<div class="scheme-wrap">',
			'#type'=>'select',
			'#options'=>$types,
			'#description'=>t('Currently it is expected that all sites will be using the same protocol'),
			'#default_value'=>(multisite_overview_scheme_type()) ? $types[0] : $types[1],
		);

		$form['onsite_url']=array(
			'#weight' => 0,
			'#title'=>t('Path to load'),
			'#type'=>'textfield',
		);

		$form['display'] = array(
			'#weight' => 100,
			'#markup'=> '<noscript>Javascript is required to load sites</noscript><div id="multisite-stage"></div>'
		);
		$form['actions']['submit'] = array(
			'#type' => 'submit',
			'#value' => t("Get URL's"),
		);	
		drupal_add_library('system', 'drupal.collapse');
		$form['#attached']['css'] = array(
			drupal_get_path('module', 'multisite_overview') . '/multisite_overview.css',
		);
		$form['#attached']['js'] = array(
			drupal_get_path('module', 'multisite_overview') . '/multisite_overview.js',
		);	
		drupal_add_js(array('multisiteOverview' => array('sites' => $folderSites)), 'setting');
		
	} else {
		$form['display'] = array(
			'#weight' => 100,
			'#markup'=> '<h3>'.t('No Multisites detected').'</h3>'
		);
	}
return $form;
}
